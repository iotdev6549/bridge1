/*
console.log("APP DEBUG");
let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");

mockMqtt.subscribe(`test`);
mockMqtt.on("message", (topic, payload, packet) => {
    console.log("TEST");
});

while(true) mockMqtt.publish("test", "HALLO");*/

import {GetAllDevicesCommandSymbol, IGetAllDevicesCommand} from "./mqtt/commands/interfaces/IGetAllDevicesCommand";
import container from "./inversify.config";
import {GetAllDevicesRequest} from "./models/communicationModels/requestModels/device/GetAllDevicesRequest";

let cmd = container.get<IGetAllDevicesCommand>(GetAllDevicesCommandSymbol);
cmd.execute(new GetAllDevicesRequest());
