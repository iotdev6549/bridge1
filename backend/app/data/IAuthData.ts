import {Auth} from "../models/Auth";

/**
 * Stellt Funktionen zum Authentifzieren von Nutzern zur Verfügung
 */
export interface IAuthData {

    /**
     * Lässt einen neuen Key zur authentifizierung erstellen
     */
    getKey(): Promise<Auth>;

    /**
     * Prüft einen Key auf seine Gültigkeit
     * @param auth Auth Objekt mit zu prüfendem Key
     */
    auth(auth: Auth): Promise<boolean>;
}