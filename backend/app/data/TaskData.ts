import {ITaskData} from "./ITaskData";
import {injectable} from "inversify";
import {Task, TaskModel} from "../models/Task.";
import {log} from "../framework/logging/log";
import {LogLevel} from "../framework/logging/LogLevel";

/**
 * @inheritDoc
 */
@injectable()
export class TaskData implements ITaskData {

    /**
    * @inheritDoc
    */
    @log(LogLevel.Method)
    create(task: Task): Promise<Task> {

        return new Promise(async (resolve) => {
            const tDoc = new Task();
            tDoc.responsible = task.responsible;
            tDoc.function = task.function;
            tDoc.created = new Date();
            const t = new TaskModel(tDoc);

            await t.save().then(value => {
                resolve(value);
            });
        });
    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    async getNext(responsible: string): Promise<Task> {
        return new Promise((resolve, reject) => {

        });
    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    finishTask(task: Task): Promise<Task> {
        return new Promise((resolve, reject) => {

        });

    }

    get(task: Task): Promise<Task | null> {
        return new Promise((resolve, reject) => {

        });    }


}