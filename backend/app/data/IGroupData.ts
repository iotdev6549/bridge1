import {Group} from "../models/Group";

export interface IGroupData {
    create(group: Group): Promise<Group>;
    update(group: Group): Promise<Group>;
    delete(group: Group): Promise<void>;
    findById(id: string): Promise<Group | undefined>;
    findByName(name: string): Promise<Group | undefined>;
    getAll(): Promise<Group[]>;
}