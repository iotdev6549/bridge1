import {Task} from "../models/Task.";

/**
 * Stellt Funktionen für Tasks bereit und bearbeitet diese in der Datenquelle
 */
export interface ITaskData {

    /**
     * Erstellt einen task in der Datenquelle
     * @param task Der zu erstellende task
     */
    create(task: Task): Promise<Task>;

    /**
     * Ermittelt den nächsten zu bearbeitenden task
     * @param responsible Empfänger des Tasks
     */
    getNext(responsible: string): Promise<Task | undefined>;

    /**
     * Schließt einen task in der Datenquelle
     * @param task Der zu schließende
     */
    finishTask(task: Task): Promise<Task>;

    /**
     * Holt sich den gesuchten Task
     * @param task Der gesuchte Task
     */
    get(task: Task): Promise<Task | null>;

}