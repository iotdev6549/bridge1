import {IGroupData} from "./IGroupData";
import {Group, GroupModel} from "../models/Group";
import {Logger} from "../framework/logging/Logger";
import {LogLevel} from "../framework/logging/LogLevel";
import {log} from "../framework/logging/log";
import {injectable} from "inversify";

@injectable()
export class GroupData implements IGroupData{

    @log(LogLevel.Method)
    create(group: Group): Promise<Group> {
        return new Promise<Group>(async (resolve, reject) => {
            const g = new GroupModel(group);
            g.name = group.name; //TODO Wieso kann hier nicht direkt gemappt werden?
            await g.save().then(value => {
                resolve(value);
            });
        });
    }
    @log(LogLevel.Method)
    delete(group: Group): Promise<void> {
        return new Promise<void>((resolve, reject) => {

        });
    }

    @log(LogLevel.Method)
    update(group: Group): Promise<Group> {
        return new Promise<Group>((resolve, reject) => {

        })
    }

    @log(LogLevel.Method)
    getAll(): Promise<Group[]> {
        return new Promise<Group[]>((resolve, reject) => {
            GroupModel.find().then(value => {
                if(value)
                    resolve(value);
                else {
                    Logger.log(LogLevel.Warning, "Es existieren keine Gruppen", "34cd54c8-9797-493a-8d10-893096d745cb", null);
                    resolve([])
                }
            });
        });
    }

    @log(LogLevel.Method)
    findById(id: string): Promise<Group | undefined> {
        return new Promise<Group|undefined>((resolve, reject) => {
            GroupModel.findById(id).then(value => {
                if(value) resolve(value);
                else {
                    Logger.log(LogLevel.Warning, `Gruppe mit der ID: ${id} nicht gefunden`, "f2b1d19b-6992-4c32-a820-9c4653930cab", null);
                    resolve(undefined);
                }
            });
        });
    }

    @log(LogLevel.Method)
    findByName(name: string): Promise<Group | undefined> {
        return new Promise<Group|undefined>((resolve, reject) => {
            GroupModel.findByName(name).then(value => {
                if(value) resolve(value);
                else {
                    Logger.log(LogLevel.Warning, `Gruppe mit dem Namen: ${name} nicht gefunden`, "30d12468-4597-40c9-8709-1edd003675b9", null);
                    resolve(undefined);
                }
            });
        })
    }

}