import {IAuthData} from "./IAuthData";
import {Auth, AuthModel} from "../models/Auth";
import {injectable} from "inversify";
import {log} from "../framework/logging/log";
import {LogLevel} from "../framework/logging/LogLevel";
const uuidv4 = require('uuid/v4');


/**
 * @inheritDoc
 */
@injectable()
export class AuthData implements IAuthData{


    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    auth(auth: Auth): Promise<boolean> {
        return new Promise((resolve, reject) => {
            AuthModel.findOne(auth).then(value => {
                if(value)
                    resolve(true);
                resolve(false);
            });
        });

    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    getKey(): Promise<Auth>{
        return new Promise((resolve) => {
            let key = uuidv4();
            let auth = new AuthModel();
            auth.authKey = key;
            auth.save().then(value => {
                resolve(value);
            })
        });
    }

}