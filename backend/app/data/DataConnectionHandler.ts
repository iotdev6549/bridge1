import {IDataConnectionHandler} from "./IDataConnectionHandler";
import mongoose from "mongoose";
import {Logger} from "../framework/logging/Logger";
import {LogLevel} from "../framework/logging/LogLevel";
import {injectable} from "inversify";

/**
 * Handhabt die Verbindung mit der MongoDB Datenbank
 */
@injectable()
export class DataConnectionHandler implements IDataConnectionHandler{
    private _connected = false;

    /**
     * Verbindet die Anwendung mit der Datenbank
     */
    connect(): Promise<void> {
        return new Promise<void>(async (resolve, reject) => {
            //Wenn die Verbindung bereits besteht kann direkt resolved werden
            if(this._connected) return resolve();

            //Datenbankverbindung wird aus den Prozess-Variablen gezogen
            if(process.env.DB)
                await mongoose.connect(process.env.DB, {useNewUrlParser: true});
            else
                return reject("Keine Datenbankverbindung ausgewählt");

            //Datenbankverbindung erfolgreich aufgebaut
            Logger.log(LogLevel.Information, `Datenbankverbindung mit ${process.env.DB} hergestellt`, "5d5fdc0d-114e-4e7c-ab93-dd5b17361296", null);
            this._connected = true;
            resolve();
        });
    }

}