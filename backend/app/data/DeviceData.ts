import {IDeviceData} from "./IDeviceData";
import {Device, DeviceModel} from "../models/Device";
import {injectable} from "inversify";
import {log} from "../framework/logging/log";
import {LogLevel} from "../framework/logging/LogLevel";
import {ILogger} from "../framework/logging/ILogger";
import {LoggingTypes} from "../framework/logging/LoggingTypes";
import container from "../inversify.config";

/**
 * @inheritDoc
 */
@injectable()
export class DeviceData implements IDeviceData {

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    create(device: Device): Promise<Device> {
        return new Promise(async (resolve) => {
            const d = new DeviceModel(device);
            await d.save().then(value => {
                resolve(value);
            });
        });
    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    get(device: Device): Promise<Device | undefined> {
        return new Promise((resolve, reject) => {
            container.get<ILogger>(LoggingTypes.Logger).log(LogLevel.Warning, 'Die Methode sollte nicht mehr aufgerufen werden', "", null)

        });
    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    getMultiple(devices: Device[]): Promise<Device[]> {
        return new Promise((resolve, reject) => {

        });
    }

    /**
     * @inheritDoc
     */
    @log(LogLevel.Method)
    updateState(device: Device): Promise<Device> {
        return new Promise((resolve, reject) => {

        });
    }

    @log(LogLevel.Method)
    getByIdentifier(identifier: string): Promise<Device | undefined> {
        return new Promise<Device|undefined>((resolve, reject) => {
            DeviceModel.findByIdentifier(identifier).then(value => {
                if(value) resolve(value);
                else {
                    container.get<ILogger>(LoggingTypes.Logger).log(LogLevel.Warning, `Das Gerät mit dem Identifier "${identifier}" konnte nicht gefunden werden`, "fbbf7f7f-b352-4aba-9632-575d2bd676c9", null)
                    resolve(undefined);
                }

            });
        });
    }

    @log(LogLevel.Method)
    getAll(): Promise<Device[]> {
        return new Promise<Device[]>((resolve, reject) => {
            DeviceModel.find().then(value => {
                resolve(value); //TODO richtige Tests machen
            });
        });
    }
}