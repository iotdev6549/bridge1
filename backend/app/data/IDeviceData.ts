import {Device} from "../models/Device";

/**
 * DataLayer für Geräte
 * Greift auf die Datenquelle für Geräte zu
 */
export interface IDeviceData {
    /**
     * Erstellt ein neues Gerät in der Datenquelle
     * @param device Das Gerät, welches erstellt werden soll
     */
    create(device: Device): Promise<Device>;

    /**
     * Aktualisiert den aktuellen Status eines Geräts in der Datenquelle
     * @param device Das zu aktualisierende Gerät
     */
    updateState(device: Device): Promise<Device>

    /**
     * Sucht ein Gerät aus der Datenquelle
     * @param device Das zu suchende Gerät
     * @deprecated Abgelöst durch findById
     */
    get(device: Device): Promise<Device | undefined>;

    /**
     * Sucht ein Gerät aus der Datenquelle
     * @param identifier Der zu suchende Identifier
     */
    getByIdentifier(identifier: string): Promise<Device | undefined>;

    /**
     * Sucht mehrere Geräte aus der Datenquelle
     * @param devices Die zu suchenden Geräte
     */
    getMultiple(devices: Device[]): Promise<Device[]>;

    getAll(): Promise<Device[]>;
}