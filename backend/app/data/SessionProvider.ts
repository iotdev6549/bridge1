const uuidv4 = require('uuid/v4');

/**
 * Stellt eine Session zur verfügung
 */
export abstract class SessionProvider {

    /**
     * Identifiziert die Server-Session
     */
    static serverSession: string = uuidv4();

    /**
     * Identifiziert die RequestParent-Session
     */
    static requestSession: string = "";

    /**
     * Generiert eine neue Session
     */
    static newRequestSession(req: any, res: any, next: () => void) {
        SessionProvider.requestSession = uuidv4();
        next();
    }
}