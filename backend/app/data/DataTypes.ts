export const DataTypes = {
    DeviceData: Symbol("DeviceData"),
    AuthData: Symbol("AuthData"),
    TaskData: Symbol("TaskData"),
    GroupData: Symbol("GroupData")
};