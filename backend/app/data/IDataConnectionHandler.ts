export interface IDataConnectionHandler {
    connect(): Promise<void>;
}

export const DataConnectionHandlerSymbol = Symbol("DataConnectionHandlerSymbol");