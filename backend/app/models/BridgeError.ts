export class BridgeError {
    message: string;
    origin: string;

    constructor(message: string, origin: string) {
        this.message = message;
        this.origin = origin;
    }
}