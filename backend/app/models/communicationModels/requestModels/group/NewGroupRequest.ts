import {RequestParent} from "../RequestParent";

export class NewGroupRequest extends RequestParent{
    name?: string;
    subGroups?: string[];
    devices?: string[];

}