const uuidv4 = require('uuid/v4');

export class RequestParent {
    requestId: string;
    sender?: string;
    respondTo?: string;

    toString(): string {
        return JSON.stringify(this);
    }

    constructor() {
        this.requestId = uuidv4();
    }

}