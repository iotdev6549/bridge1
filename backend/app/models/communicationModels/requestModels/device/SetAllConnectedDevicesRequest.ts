export class SetAllConnectedDevicesRequest {
    connectedDevices: string[];

    constructor(connectedDevices: string[]) {
        this.connectedDevices = connectedDevices;
    }
    
}