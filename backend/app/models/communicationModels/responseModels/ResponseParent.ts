import {BridgeError} from "../../BridgeError";
import {IMqttHandler, MqttHandlerSymbol} from "../../../mqtt/IMqttHandler";
import container from "../../../inversify.config";

export class ResponseParent {
    responseFor?: string;
    done = false;
    hasError = false;
    error?: BridgeError;

    toString(): string {
        return JSON.stringify(this);
    }

}