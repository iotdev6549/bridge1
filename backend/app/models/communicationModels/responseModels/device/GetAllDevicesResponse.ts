import {ResponseParent} from "../ResponseParent";
import {Device} from "../../../Device";

export class GetAllDevicesResponse extends ResponseParent {
    devices?: Device[];
}