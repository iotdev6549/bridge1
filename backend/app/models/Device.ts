import {ModelType, prop, staticMethod, Typegoose} from "typegoose";

/**
 * Repräsentiert ein Gerät in der Datenbank
 */
export class Device extends Typegoose{

    /**
     * Datenbannk-ID des Gerätes
     */
    @prop()
    id?: string;

    /**
     * Name des Gerätes
     */
    @prop()
    name?: string;

    /**
     * Einmaliger Identifier des Gerätes
     * Unterscheidet sich vom Datenbankidentifier, da dieser fest im Gerät gesetzt wurde
     */
    @prop()
    identifier?: string;

    /**
     * Typ des Gerätes
     * Repräsentiert durch enum DevicesTypes
     */
    @prop()
    type?: string;

    /**
     * Aktueller State des Gerätes
     */
    @prop()
    state?: any;

    /**
     * Funktionen, die das Gerät zur Verfügung stellt
     */
    @prop()
    functions?: any[];

    @staticMethod
    static findByIdentifier(this: ModelType<Device> & typeof Device, identifier: string) {
        return this.findOne({ identifier });
    }

}

export enum DeviceTypes {
    Lamp = "Lamp"
}

export const DeviceModel = new Device().getModelForClass(Device);
