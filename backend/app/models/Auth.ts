import {prop, Typegoose} from "typegoose";

/**
 * Stellt Auth in der Datenbank dar
 */
export class Auth extends Typegoose{

    /**
     * Authentifizierungsschlüssel
     */
    @prop()
    authKey?: string;
}

export const AuthModel = new Auth().getModelForClass(Auth);
