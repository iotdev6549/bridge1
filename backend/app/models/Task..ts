import {prop, Typegoose} from "typegoose";

/**
 * Stellt einen Task in der Datenbank dar
 */
export class Task extends Typegoose{

    /**
     * Datenbank-ID
     */
    @prop()
    id?: string;

    /**
     * Geräte-ID, welches die Aufgabe ausführen soll
     */
    @prop()
    responsible?: string;

    /**
     * Funktion, die ausgeführt werden soll
     */
    @prop()
    function?: any;

    /**
     * Ob die Aufgabe bereits abgeschlossen ist
     */
    @prop()
    finished: boolean = false;

    /**
     * Wann die Aufgabe erstellt wurde
     */
    @prop()
    created?: Date;
}
export const TaskModel = new Task().getModelForClass(Task);
