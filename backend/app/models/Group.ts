import {ModelType, prop, staticMethod, Typegoose} from "typegoose";
import {Device} from "./Device";

export class Group extends Typegoose {

    @prop()
    name?: string;

    @prop()
    subGroups?: string[]; //TODO Kann mongoDb die referenzen bilden?

    @prop()
    devices?: string[];

    @staticMethod
    static findByName(this: ModelType<Group> & typeof Group, name: string) {
        return this.findOne({ name });
    }

}

export const GroupModel = new Group().getModelForClass(Group);
