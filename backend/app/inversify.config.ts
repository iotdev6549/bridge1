import "reflect-metadata"; //https://stackoverflow.com/questions/37534890/inversify-js-reflect-hasownmetadata-is-not-a-function
import {Container} from "inversify";
import {bindContainer} from "./bindContainer";


let container = new Container();
bindContainer(container);
export default container;