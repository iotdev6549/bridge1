import {LogLevel} from "./framework/logging/LogLevel";
import {Logger} from "./framework/logging/Logger";
import {Server} from "./Server";

process.env.DB = "mongodb://localhost:27017/bridge1";

process.on('uncaughtException', function(err) {
    Logger.log(LogLevel.Error, "Es ist ein unerwarteter Fehler aufgetreten", "86499c0c-f1c2-46b7-879c-01a5b322f850", err);
});

Logger.log(LogLevel.Information, "Bridge fckn' 1 wird gestartet...", "", null);

new Server().startUp().then(() => {
    Logger.log(LogLevel.Information, "Server erfolgreich gestartet", "", null);
});