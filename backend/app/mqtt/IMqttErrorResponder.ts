import {IErrorResponder} from "../framework/logic/IErrorResponder";

export interface IMqttErrorResponder extends IErrorResponder{
}

export const MqttErrorResponderSymbol = Symbol("MqttErrorResponderSymbol");