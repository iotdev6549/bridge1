import {INewGroupCommand} from "./interfaces/INewGroupCommand";
import {NewGroupRequest} from "../../models/communicationModels/requestModels/group/NewGroupRequest";
import {log} from "../../framework/logging/log";
import {LogLevel} from "../../framework/logging/LogLevel";
import {requestWithValidBody} from "../../validation/requestWithValidBody";
import {NewGroupRequestValidator} from "../../validation/group/NewGroupRequestValidator";
import {requestModel} from "../../framework/base/decorators/requestModel";
import {inject, injectable} from "inversify";
import {DataTypes} from "../../data/DataTypes";
import {IGroupData} from "../../data/IGroupData";
import {Group} from "../../models/Group";
import {Logger} from "../../framework/logging/Logger";
import {IMqttHandler, MqttHandlerSymbol} from "../IMqttHandler";
import {NewGroupResponse} from "../../models/communicationModels/responseModels/group/NewGroupResponse";
import {MqttErrorResponder} from "../MqttErrorResponder";

@injectable()
export class NewGroupCommand implements INewGroupCommand{

    constructor(
        @inject(MqttHandlerSymbol) private _handler: IMqttHandler,
        @inject(DataTypes.GroupData) private _groupData: IGroupData
    ) {
    }

    @requestWithValidBody<NewGroupRequest>(new NewGroupRequestValidator(), new MqttErrorResponder())
    @log(LogLevel.Method)
    execute(@requestModel req: NewGroupRequest): void {
        let group = new Group();
        group.name = req.name;
        this._groupData.create(group).then(value => {
            Logger.log(LogLevel.Information, "Neue Gruppe erstellt", "5edf2212-2541-4c8e-810a-9e6661f1ff3e", value);
            let response = new NewGroupResponse();
            response.done = true;
            this._handler.publish(`device/${req.sender}/response/NewGroupResponse`, response.toString(), null, () => {});
        });

    }

}