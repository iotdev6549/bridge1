import {ICommand} from "./ICommand";
import {GetAllDevicesRequest} from "../../../models/communicationModels/requestModels/device/GetAllDevicesRequest";

export interface IGetAllDevicesCommand extends ICommand<GetAllDevicesRequest>{}

export const GetAllDevicesCommandSymbol = Symbol("GetAllDevicesCommandSymbol");