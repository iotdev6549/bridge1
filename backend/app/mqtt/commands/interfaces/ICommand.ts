export interface ICommand<TRequest> {
    execute(req: TRequest): void;
}