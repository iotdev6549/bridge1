import {ICommand} from "./ICommand";
import {NewGroupRequest} from "../../../models/communicationModels/requestModels/group/NewGroupRequest";

export interface INewGroupCommand extends ICommand<NewGroupRequest>{}

export const NewGroupCommandSymbol = Symbol("NewGroupCommand");