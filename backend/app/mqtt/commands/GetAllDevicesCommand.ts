import {inject, injectable} from "inversify";
import {IGetAllDevicesCommand} from "./interfaces/IGetAllDevicesCommand";
import {IMqttHandler, MqttHandlerSymbol} from "../IMqttHandler";
import {DataTypes} from "../../data/DataTypes";
import {IDeviceData} from "../../data/IDeviceData";
import {GetAllDevicesRequest} from "../../models/communicationModels/requestModels/device/GetAllDevicesRequest";
import {log} from "../../framework/logging/log";
import {requestModel} from "../../framework/base/decorators/requestModel";
import {LogLevel} from "../../framework/logging/LogLevel";
import {requestWithValidBody} from "../../validation/requestWithValidBody";
import {GetAllDevicesRequestValidator} from "../../validation/device/GetAllDevicesRequestValidator";
import {GetAllDevicesResponse} from "../../models/communicationModels/responseModels/device/GetAllDevicesResponse";
import {MqttErrorResponder} from "../MqttErrorResponder";


/**
 * Command um alle existierenden Geräte zu erhalten
 */
@injectable()
export class GetAllDevicesCommand implements IGetAllDevicesCommand {

    constructor(
        @inject(MqttHandlerSymbol) private _handler: IMqttHandler,
        @inject(DataTypes.DeviceData) private _deviceData: IDeviceData
    ) {
    }

    /**
     * Führt den Request aus
     * Schickt alle Devices per Request an das anfragende Gerät zurück
     * @param req Requestinformationen
     */
    @requestWithValidBody<GetAllDevicesRequest>(new GetAllDevicesRequestValidator(), new MqttErrorResponder())
    @log(LogLevel.Method)
    execute(@requestModel req: GetAllDevicesRequest): void {
        this._deviceData.getAll().then(devices => {
            let response = new GetAllDevicesResponse();
            response.devices = devices;
            response.done = true;
            this._handler.publish(`device/${req.sender}/response/${req.requestId}`, JSON.stringify(response), null, () => {});
        }).catch(reason => {
            console.log(reason);
        });
    }

}