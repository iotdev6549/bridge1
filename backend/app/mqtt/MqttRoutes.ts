import {IMqttHandler, MqttHandlerSymbol} from "./IMqttHandler";
import {inject, injectable} from "inversify";
import {IMqttRoutes} from "./IMqttRoutes";
import {DeviceCommands} from "./topics/commands/DeviceCommands";
import {GetAllDevicesCommandSymbol, IGetAllDevicesCommand} from "./commands/interfaces/IGetAllDevicesCommand";
import {log} from "../framework/logging/log";
import {LogLevel} from "../framework/logging/LogLevel";
import container from "../inversify.config";
import {GetAllDevicesRequest} from "../models/communicationModels/requestModels/device/GetAllDevicesRequest";
import {GroupCommands} from "./topics/commands/GroupCommands";
import {NewGroupCommand} from "./commands/NewGroupCommand";
import {INewGroupCommand, NewGroupCommandSymbol} from "./commands/interfaces/INewGroupCommand";

/**
 * Definiert die MQTT Routen
 */
@injectable()
export class MqttRoutes implements IMqttRoutes {
    private _registered = false;


    /**
     * Initialisiert die Route Klasse
     * erhält die Commands per Dependency Injection
     * @param handler MqttHandler
     * @param _getAllDevicesCommand GetAllDevicesCommand
     * @param _newGroupCommand NewGroupCommand
     */
    constructor(
        @inject(MqttHandlerSymbol) private handler: IMqttHandler,
        @inject(GetAllDevicesCommandSymbol) private _getAllDevicesCommand: IGetAllDevicesCommand,
        @inject(NewGroupCommandSymbol) private _newGroupCommand: INewGroupCommand
    ) {
    }

    /**
     * Registriert sämtliche Routen
     */
    @log(LogLevel.Method)
    register() {
        if(this._registered) return;
        //Commands
        this.handler.registerRoute(DeviceCommands.GetAllDevicesCommand, this._getAllDevicesCommand);
        this.handler.registerRoute(GroupCommands.NewGroupCommand, this._newGroupCommand);
        this._registered = true;
    }
}