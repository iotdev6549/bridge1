export interface IMqttHandler {
    connect(): Promise<void>;
    publish(topic: string, message: string, options: any, callback: () => void): void;
    registerRoute(route: string, callback: any): void;
    startListening(): void;
}

export const MqttHandlerSymbol = Symbol("MqttHandler");