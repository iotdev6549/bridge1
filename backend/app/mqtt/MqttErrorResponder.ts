import {IMqttErrorResponder} from "./IMqttErrorResponder";
import {RequestParent} from "../models/communicationModels/requestModels/RequestParent";
import {BridgeError} from "../models/BridgeError";
import {injectable} from "inversify";
import {IMqttHandler, MqttHandlerSymbol} from "./IMqttHandler";
import {ResponseParent} from "../models/communicationModels/responseModels/ResponseParent";
import container from "../inversify.config";
import {Logger} from "../framework/logging/Logger";
import {LogLevel} from "../framework/logging/LogLevel";
import {log} from "../framework/logging/log";

/**
 * Habdhabt Errors und schickt diese an die Request-Sender unter der ausgeführten RequestID zurück
 */
@injectable()
export class MqttErrorResponder implements IMqttErrorResponder{

    /**
     * Schickt den Error an das anfragende Gerät
     * @param request Der auszuführende Request (hier wird auf die Sender-ID zugegriffen)
     * @param error Der Error, der aufgetreten ist
     */
    @log(LogLevel.Method)
    respond(request: RequestParent, error: BridgeError): void {
        if(!request.sender || request.sender == "") {
            return Logger.log(LogLevel.Warning, "Konnte Fehler nicht verschicken, da kein Sender vorhanden ist", "782d8ced-b416-4b44-a9c8-fcbb151e76e9", null);
        }
        let response = new ResponseParent();
        response.done = false;
        response.hasError = true;
        response.error = error;
        container.get<IMqttHandler>(MqttHandlerSymbol).publish(`device/${request.sender}/response/${request.requestId}`, response.toString(), null, () => {});
    }

}