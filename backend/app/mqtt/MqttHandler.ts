import {IMqttHandler} from "./IMqttHandler";
import mqtt, {MqttClient} from "mqtt";
import {inject, injectable} from "inversify";
import {log} from "../framework/logging/log";
import {LogLevel} from "../framework/logging/LogLevel";
import {MqttConfigSymbol} from "../config/mqtt.config";
import {Logger} from "../framework/logging/Logger";
import {ICommand} from "./commands/interfaces/ICommand";

@injectable()
export class MqttHandler implements IMqttHandler{
    private static client?: MqttClient;
    private static routeCallbacks: { [route: string]: any} = { };

    constructor(
        @inject(MqttConfigSymbol) private _config: any
    ) {
    }

    @log(LogLevel.Method)
    connect(): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            try {
                let url = this._config.url;
                MqttHandler.client = mqtt.connect(url);
                MqttHandler.client.on('connect', function () {
                    Logger.log(LogLevel.Information, `MQTT-Verbindung mit ${url} aufgebaut`, "3a07c29f-7a8f-4a18-b9fd-78d36a69ea6e", null)
                    resolve();
                });
            }catch (e) {
                reject(e);
            }
        })

    }

    @log(LogLevel.Method)
    publish(topic: string, message: string, options: any = null, callback: () => void = () => {}): void {
        if(!options) {
            //TODO implementieren
        }
        Logger.log(LogLevel.Verbose,`Publish - Topic: ${topic}`, "17362e4f-2d4c-4aab-a73c-6535ea4b3b5a", null);
        if(!MqttHandler.client) throw "Der Client muss vor dem Publishen verbunden werden";
        MqttHandler.client.publish(topic, message);
    }

    @log(LogLevel.Method)
    registerRoute<T>(route: string, command: ICommand<T>): void {
        if(!MqttHandler.client) throw "Der Client muss vor dem Registrieren einer Route verbunden werden";
        Logger.log(LogLevel.Verbose, `Registriere Topic: ${route}`, "14c355c5-0f55-4943-b575-d0c05ba63b35", null);
        MqttHandler.client.subscribe(route);
        MqttHandler.routeCallbacks[route] = command;
    }

    @log(LogLevel.Method)
    startListening(): void {
        if(!MqttHandler.client) throw "Der Client muss vor dem Listening verbunden werden";
        MqttHandler.client.on("message", (topic, payload, packet) => {
            Logger.log(LogLevel.Verbose, `Nachricht erhalten. Topic: ${topic} Message: ${payload}`, "9dbfe754-acea-423c-b80d-5f895da3e518", null);
            let command = MqttHandler.routeCallbacks[topic];

            //Topic ist ein Buffer, der zunächst zum String konvertiert werden muss
            //bevor er zum JSON umgewandelt werden kann
            command.execute(JSON.parse(payload.toString()));
        });
    }



}