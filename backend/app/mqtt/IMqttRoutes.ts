export interface IMqttRoutes {
    register(): void;
}

export const MqttRoutesSymbol = Symbol("MqttRoutes");