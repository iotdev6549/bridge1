export interface IPutMethod<T> {
    execute(t?: T): Promise<T>;
}