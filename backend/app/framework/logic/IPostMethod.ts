export interface IPostMethod<T> {
    execute(t: T): Promise<T>;
}