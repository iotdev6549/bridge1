export interface IGetMethod<TRequest, TResult> {
    execute(t: TRequest): Promise<TResult | undefined>;
}