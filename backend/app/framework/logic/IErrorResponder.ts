import {RequestParent} from "../../models/communicationModels/requestModels/RequestParent";
import {BridgeError} from "../../models/BridgeError";

export interface IErrorResponder {
    respond(request: RequestParent, error: BridgeError): void;
}