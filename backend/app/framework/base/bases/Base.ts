import {ILogger} from "../../logging/ILogger";
import {LogLevel} from "../../logging/LogLevel";
import {LoggingTypes} from "../../logging/LoggingTypes";
import container from "../../../inversify.config";

export abstract class Base implements ILogger{
    log(level: LogLevel, message: string | number, origin: string = "BASE", attachment: any = null): void {
        container.get<ILogger>(LoggingTypes.Logger).log(level, message, origin, attachment);
    }

}