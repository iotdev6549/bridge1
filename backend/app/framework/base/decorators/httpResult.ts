import "reflect-metadata";

export function httpResult(target: object, propertyKey: string | symbol, parameterIndex: number) {
    Reflect.defineMetadata(HttpResultSymbol, parameterIndex, target, propertyKey);
}

export const HttpResultSymbol = Symbol("HttpResultSymbol");