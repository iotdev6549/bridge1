import "reflect-metadata";

export function httpRequest(target: object, propertyKey: string | symbol, parameterIndex: number) {
    Reflect.defineMetadata(HttpRequestSymbol, parameterIndex, target, propertyKey);
}

export const HttpRequestSymbol = Symbol("HttpRequestSymbol");