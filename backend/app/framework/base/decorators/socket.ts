import "reflect-metadata";

export function socket(target: object, propertyKey: string | symbol, parameterIndex: number) {
    Reflect.defineMetadata(SocketSymbol, parameterIndex, target, propertyKey);
}

export const SocketSymbol = Symbol("SocketSymbol");