import "reflect-metadata";

/**
 * Descriptor für Errormethoden als Prameter
 * @param target
 * @param propertyKey
 * @param parameterIndex
 */
export function error(target: object, propertyKey: string | symbol, parameterIndex: number) {
    Reflect.defineMetadata("errorMethodIndex", parameterIndex, target, propertyKey);
}