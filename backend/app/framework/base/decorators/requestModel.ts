import "reflect-metadata";

export function requestModel(target: object, propertyKey: string | symbol, parameterIndex: number) {
    Reflect.defineMetadata(RequestModelSymbol, parameterIndex, target, propertyKey);
}

export const RequestModelSymbol = Symbol("RequestModelSymbol");