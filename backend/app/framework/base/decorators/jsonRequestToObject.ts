import "reflect-metadata";



export function jsonRequestToObject<T>(){
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor)  {
        const originalValue = descriptor.value;
        descriptor.value = async function (...args: any[]) {
                let reqIndex = Reflect.getOwnMetadata(RequestModelSymbol, target, propertyKey);
                args[reqIndex] = JSON.parse(args[reqIndex]) as T;
                console.log(args);
                return originalValue.apply(this, args);
            }
    }
}

export const RequestModelSymbol = Symbol("RequestModelSymbol");