// import container from "../../inversify.config";
// import {Auth} from "../../models/Auth";
// import {IAuthData} from "../../data/IAuthData";
// import {DataTypes} from "../../data/DataTypes";
// import {RequestTypes} from "./RequestTypes";
// import {HttpRequestSymbol} from "../base/decorators/httpRequest";
// import {HttpResultSymbol} from "../base/decorators/httpResult";
// import {socket, SocketSymbol} from "../base/decorators/socket";
//
// export function requireAuth(authType: RequestTypes) {
//     return function (target: any, propertyName: string, descriptor: PropertyDescriptor){
//
//         let method = descriptor.value;
//         descriptor.value = async function (...args: any[]) {
//             if (authType == RequestTypes.REST) {
//                 let requestIndex = Reflect.getOwnMetadata(HttpRequestSymbol, target, propertyName);
//                 let resultIndex = Reflect.getOwnMetadata(HttpResultSymbol, target, propertyName);
//                 let request = args[requestIndex];
//                 let result = args[resultIndex];
//
//                 if ("authkey" in request.headers) {
//                     let auth = new Auth();
//                     auth.authKey = request.headers["authkey"];
//                     try {
//                         let res = await container.get<IAuthData>(DataTypes.AuthData).auth(auth);
//                         if (res)
//                             return method.apply(this, args);
//                     } catch (error) {
//                         result.sendStatus(401);
//                         return;
//                     }
//                 }
//                 result.sendStatus(401);
//             } else if (authType == RequestTypes.Socket) {
//                 let socketIndex = Reflect.getOwnMetadata(SocketSymbol, target, propertyName);
//
//                 let socketWrap = args[socketIndex];
//
//                 let socket = socketWrap.socket;
//                 if ("handshake" in socket && "query" in socket.handshake && "authkey" in socket.handshake.query) {
//
//                     let authKey = socket.handshake.query.authkey;
//                     try {
//                         let auth = new Auth();
//                         auth.authKey = authKey;
//                         await container.get<IAuthData>(DataTypes.AuthData).auth(auth);
//                         return method.apply(this, args);
//                     } catch (error) {
//                         let em = new ErrorMessage();
//                         em.status = ErrorCodes.AuthenticationFailed;
//                         socketWrap.emitError(em);
//                         return;
//                     }
//
//                 } else {
//                     let em = new ErrorMessage();
//                     em.status = ErrorCodes.AuthenticationFailed;
//                     em.content = "Es wurde kein Authkey übergeben";
//                     socketWrap.emitError(em);
//                 }
//             }
//
//         }
//     }
// }