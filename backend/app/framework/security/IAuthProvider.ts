/**
 * Stellt Funktionen zur Authentifizierung eines Nutzers zur Verfügung
 */
export interface IAuthProvider {

    /**
     * Authentifiziert einen Nutzer
     * @param req Request, in dem die Nutzerdaten enthalten sind
     * @param res Result, an das ggf. gesendet wird
     * @param success Wird im Erfolgsfall aufgerufen
     */
    auth(req: any, res: any, success: () => void): void;

    /**
     * Schickt einen Error an den Nutzer, dass dieser nicht authentifiziert ist
     */
    sendAuthError(): void;

    /**
     * Gibt an, ob der aktuelle Nutzer authentifiziert ist
     */
    isAuthorized: boolean;
}