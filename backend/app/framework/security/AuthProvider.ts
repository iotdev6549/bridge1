import container from "../../inversify.config";
import {IAuthData} from "../../data/IAuthData";
import {DataTypes} from "../../data/DataTypes";
import {Auth} from "../../models/Auth";

/**
 * @inheritDoc
 */
export abstract class AuthProvider {
    /**
     * @inheritDoc
     */
    static isAuthorized: boolean = false;

    static _req: any;
    static _res: any;

    /**
     * @inheritDoc
     */
    static auth(req: any, res: any): Promise<boolean>{
        return new Promise<boolean>((resolve) => {
            AuthProvider._req = req;
            AuthProvider._res = res;

            req.isAuthorized = false;
            resolve();
        });

    }

}