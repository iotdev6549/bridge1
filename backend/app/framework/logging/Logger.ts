import {ILogger} from "./ILogger";
import {LogLevel} from "./LogLevel";
import container from "../../inversify.config";
import {LoggingTypes} from "./LoggingTypes";

export abstract class Logger {
    static log(level: LogLevel, message: string | number, origin: string, attachment: any): void {
        container.get<ILogger>(LoggingTypes.Logger).log(level, message, origin, attachment);
    }

}