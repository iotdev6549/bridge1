import {ILogger} from "./ILogger";
import {LogLevel} from "./LogLevel";
import {injectable} from "inversify";
import chalk from "chalk";

/**
 * Logger für Logeinträge in der JS Console
 */
@injectable()
export class ConsoleLogger implements ILogger {

    /**
     * Loggt einen Eintrag
     * @param level Logging Level bestimmt die "Wichtigkeit"
     * @param message Die Nachricht, die geloggt werden soll
     * @param origin Der Herkunft der Nachricht
     * @param attachment Möglicher Anhang der Nachricht
     */
    log(level: LogLevel, message: string, origin: string, attachment: any): void {
        let log = `${level.toString()} | ${message}`;
        switch (level) {
            case LogLevel.Verbose:
                console.log(chalk.white(log));
                break;
            case LogLevel.Method:
                console.log(chalk.white(log));
                break;
            case LogLevel.Information:
                console.log(chalk.blue(log));
                break;
            case LogLevel.Warning:
                console.log(chalk.yellow(log));
                break;
            case LogLevel.Error:
                console.log(chalk.red(log));
                break;
            case LogLevel.Debug:
                console.log(chalk.cyan(log));
                if(attachment) console.log(attachment);
                break;


        }
    }
}