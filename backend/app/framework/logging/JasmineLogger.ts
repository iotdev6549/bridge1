import {ILogger} from "./ILogger";
import {LogLevel} from "./LogLevel";

/**
 * Basislogger für generelle Log-Angelegengeiten
 * Verteilt die Logeinträge an alle registrierten Logger weiter
 */
export class JasmineLogger implements ILogger{

    /**
     * Initialisiert den Logger
     * @param _logger Liste aller Logger, die mitzeichnen sollen
     */
    constructor(private _logger: ILogger[]) {
    }

    /**
     * Loggt den Eintrag
     * @param level Logging Level bestimmt die "Wichtigkeit"
     * @param message Die Nachricht, die geloggt werden soll
     * @param origin Der Herkunft der Nachricht
     * @param attachment Möglicher Anhang der Nachricht
     */
    log(level: LogLevel, message: string, origin: string, attachment: any): void {
        this._logger.forEach(x => x.log(level, message, origin, attachment))
    }

}