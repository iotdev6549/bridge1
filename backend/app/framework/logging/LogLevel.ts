/**
 * Level zum Loggen
 * Teilt die "Wichtigkeit" des Logeintrags mit
 */
export enum LogLevel {
    Verbose = "V",
    Method = "M",
    Information = "I",
    Warning = "W",
    Error = "E",
    Debug = "D"
}