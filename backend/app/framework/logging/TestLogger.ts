import {ILogger} from "./ILogger";
import {LogLevel} from "./LogLevel";
import {injectable} from "inversify";

@injectable()
export class TestLogger implements ILogger{
    log(level: LogLevel, message: string, origin: string, attachment: any): void {
    }

}