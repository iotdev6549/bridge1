import {LogLevel} from "./LogLevel";
import container from "../../inversify.config";
import {ILogger} from "./ILogger";
import {LoggingTypes} from "./LoggingTypes";

export function log(logLevel: LogLevel){
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor)  {
        const originalValue = descriptor.value;

        descriptor.value = function (...args: any[]) {
            let message = `${target.constructor.name} => ${propertyKey}`;
            let logger = container.get<ILogger>(LoggingTypes.Logger);
            logger.log(logLevel, message, "f98db6a1-3d94-4ee2-a520-abafb3eae5d0", null);
            return originalValue.apply(this, args);
        }
    }
}