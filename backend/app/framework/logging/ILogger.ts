import {LogLevel} from "./LogLevel";

/**
 * Interface für sämtliche Logger
 */
export interface ILogger {
    /**
     * Loggt einen Eintrag
     * @param level Logging Level bestimmt die "Wichtigkeit"
     * @param message Die Nachricht, die geloggt werden soll
     * @param origin Der Herkunft der Nachricht
     * @param attachment Möglicher Anhang der Nachricht
     */
    log(level: LogLevel, message: string | number, origin: string, attachment: any): void;
}