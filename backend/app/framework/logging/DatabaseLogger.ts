import {ILogger} from "./ILogger";
import {LogLevel} from "./LogLevel";
import {LogEntry} from "./LogEntry";
import {SessionProvider} from "../../data/SessionProvider";

/**
 * @inheritDoc
 */
export class DatabaseLogger implements ILogger {
    
    private _cache: LogEntry[] = [];

    /**
     * @inheritDoc
     * Loggt in der Datenbank
     */
    log(level: LogLevel = LogLevel.Verbose, message: string, origin: string, attachment?: any): void {
        let entry = new LogEntry(new Date(), message, SessionProvider.serverSession, SessionProvider.requestSession, origin, attachment);
        this._cache.push(entry);

        //Wenn noch keine Datenbankverbindung besteht, wird der Eintrag gecached, bis eine Verbindung aufgebaut wurde
        // if(!DatabaseProvider.connected) return;

        //Cache abbauen
        while(this._cache.length > 0) {

        }

    }

}