export const LoggingTypes = {
    Logger: Symbol("Logger"),
    JasmineLogger: Symbol("JasmineLogger"),
    DatabaseLogger: Symbol("DatabaseLogger")
};