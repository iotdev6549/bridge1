/**
 * Model für einen Logeintrag
 */
export class LogEntry {
    time: Date;
    message: string;
    serverSession: string;
    requestSession: string;
    origin: string;
    attachment: any;

    /**
     * Intialisiert das Objekt
     * @param time Zeit des Eintrages
     * @param message Nachricht
     * @param serverSession Aktuelle Server-Session
     * @param requestSession Aktuelle Request-Session
     * @param origin Herkunft des Eintrags
     * @param attachment Anhang
     */
    constructor(time: Date, message: string, serverSession: string, requestSession: string, origin: string, attachment: any) {
        this.time = time;
        this.message = message;
        this.serverSession = serverSession;
        this.requestSession = requestSession;
        this.origin = origin;
        this.attachment = attachment;
    }
}