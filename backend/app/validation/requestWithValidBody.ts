import "reflect-metadata";
import {IValidator} from "./IValidator";
import {RequestModelSymbol} from "../framework/base/decorators/requestModel";
import {Logger} from "../framework/logging/Logger";
import {LogLevel} from "../framework/logging/LogLevel";
import {IErrorResponder} from "../framework/logic/IErrorResponder";


export function requestWithValidBody<T>(validator: IValidator<T>, errorResponder: IErrorResponder){
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor)  {
        const originalValue = descriptor.value;

        descriptor.value = async function (...args: any[]) {
            Logger.log(LogLevel.Verbose, "Validiere Request", "cee68db8-4885-4cff-b032-db2fa4b1bdef", null);
            let reqIndex = Reflect.getOwnMetadata(RequestModelSymbol, target, propertyKey);
            if(reqIndex == undefined) return Logger.log(LogLevel.Warning, "Es wurde kein Request Decorator gefunden", "d5badbb3-277e-421d-ac65-6cf3c1968d84", null);
            let req = args[reqIndex];
            try {
                await validator.validate(req);
            } catch (e) {
                Logger.log(LogLevel.Warning, "Es wurde ein fehlerhafter Request übergeben", "80657c0a-0d08-4aaf-a290-04a44dd66224", e);
                return errorResponder.respond(req, e);
            }
            return originalValue.apply(this, args);

        }
    }
}