import {IValidator} from "../IValidator";
import {NewGroupRequest} from "../../models/communicationModels/requestModels/group/NewGroupRequest";
import container from "../../inversify.config";
import {IDeviceData} from "../../data/IDeviceData";
import {DataTypes} from "../../data/DataTypes";
import {BridgeError} from "../../models/BridgeError";
import {IGroupData} from "../../data/IGroupData";

/**
 * Validator für NewGroupRequest`s
 * Muss vorhanden sein: sender, name
 * Der name darf nicht bereits existieren
 * Wenn Gruppen übergeben werden, müssen diese existieren
 * Wenn Geräte übergeben werden, müssen diese existieren
 */
export class NewGroupRequestValidator implements IValidator<NewGroupRequest> {
    validate(obj: NewGroupRequest): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            let groupData = container.get<IGroupData>(DataTypes.GroupData);
            let deviceData = container.get<IDeviceData>(DataTypes.DeviceData);
            //Sender darf nicht leer sein
            if(obj.sender == null || obj.sender == "") return reject(new BridgeError("Das Feld 'sender' muss gefüllt sein", "e8b4ac0e-9696-4f71-9ac9-0c703f91067c"));

            //Name muss gefüllt sein
            if(obj.name == null || obj.name == "") return reject(new BridgeError("Das Feld 'name' muss gefüllt sein", "0af4a6a1-0607-4ce8-a425-da411089cd7a"));

            try {
                //Sender muss existieren
                let device = await deviceData.getByIdentifier(obj.sender);
                if(!device) return reject(new BridgeError("Der angebene 'sender' existiert nicht", "a450dc27-4aa3-46e4-9d2d-22ac971cdb99"));

                //Name darf nicht existieren
                let group = await groupData.findByName(obj.name);
                if(group) return reject(new BridgeError("Der Gruppenname existiert bereits", "8916eb70-900a-4c8a-8f42-059212d3c9aa"));

                //Es müssen keine Sub-Gruppen genannt werden, wenn jedoch welche genannt werden, müssen diese auch existieren
                if(obj.subGroups)
                    for (let i = 0; i < obj.subGroups.length; i++)
                        if(!await groupData.findById(obj.subGroups[i])) return reject(new BridgeError(`Die Gruppe ${obj.subGroups[i]} existiert nicht`, "6ce92b2f-c982-43f5-aa1b-04783ec6e816"));

                //Es müssen keine Geräte genannt werden, wenn jedoch welche genannt werden, müssen diese auch existieren
                if(obj.devices)
                    for (let i = 0; i < obj.devices.length; i++)
                        if(!await deviceData.getByIdentifier(obj.devices[i])) return reject(new BridgeError(`Das Gerät ${obj.devices[i]} existiert nicht`, "315d7bdb-0c58-420a-a40f-9f84d634656e"));


            } catch (e) {
                //Es dürfen keine Exceptions auftreten
                return reject(new BridgeError("Es ist ein unerwarteter Fehler aufgetreten", "823c8ab2-cc59-4d3a-8918-58911eafacaf"));
            }

            //Erfolg
            resolve();

        });
    }

}
