import {IValidator} from "../IValidator";
import {GetAllDevicesRequest} from "../../models/communicationModels/requestModels/device/GetAllDevicesRequest";
import container from "../../inversify.config";
import {DataTypes} from "../../data/DataTypes";
import {IDeviceData} from "../../data/IDeviceData";
import {BridgeError} from "../../models/BridgeError";

/**
 * Validiert einen GetAllDevicesRequest
 * Der Request muss folgende Bedingungen erfüllen
 * 1. Gültiger Sender
 */
export class GetAllDevicesRequestValidator implements IValidator<GetAllDevicesRequest>{
    validate(obj: GetAllDevicesRequest): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            //Sender darf nicht leer sein
            if(obj.sender == null || obj.sender == "") return reject(new BridgeError("Das Feld 'sender' muss gefüllt sein", "94b34907-ba9e-4e14-a397-055fbdebe4a2"));
            try {
                //Sender muss existieren
                let device = await container.get<IDeviceData>(DataTypes.DeviceData).getByIdentifier(obj.sender);
                if(!device) return reject(new BridgeError("Der angebene 'sender' existiert nicht", "3cc14c9f-bf4d-41dc-ba95-3705342fef84"));
            } catch (e) {
                //Es dürfen keine Exceptions auftreten
                return reject(new BridgeError("Es ist ein unerwarteter Fehler aufgetreten", "d3d8f593-fd79-44a6-bb96-51484e0dd9d1"));
            }
            resolve();
        });
    }

}