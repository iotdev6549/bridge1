import {IValidator} from "../IValidator";
import {NewTaskRequest} from "../../models/communicationModels/requestModels/task/NewTaskRequest";
import container from "../../inversify.config";
import {IDeviceData} from "../../data/IDeviceData";
import {DataTypes} from "../../data/DataTypes";
import {Device} from "../../models/Device";

export class NewTaskRequestValidator implements IValidator<NewTaskRequest>{
    validate(obj: NewTaskRequest): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            if(!obj.responsible || obj.responsible.length < 5) reject(["Fehler bei Responsible"]);

            let deviceData = container.get<IDeviceData>(DataTypes.DeviceData);
            let getDevice = new Device();
            getDevice.identifier = obj.responsible;
            try {
                let device = await deviceData.get(getDevice);
                if(!device) reject(["Das Gerät existiert nicht"]);
            } catch (e) {
                reject(["Es ist ein interner Fehler aufgetreten"]);
            }
            resolve(true);
        });
    }

}