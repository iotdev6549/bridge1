import {IValidator} from "../IValidator";
import {NewTaskRequest} from "../../models/communicationModels/requestModels/task/NewTaskRequest";
import container from "../../inversify.config";
import {IDeviceData} from "../../data/IDeviceData";
import {DataTypes} from "../../data/DataTypes";
import {Device} from "../../models/Device";
import {FinishTaskRequest} from "../../models/communicationModels/requestModels/task/FinishTaskRequest";
import {ITaskData} from "../../data/ITaskData";
import {Task} from "../../models/Task.";

export class FinishTaskRequestValidator implements IValidator<FinishTaskRequest>{
    validate(obj: FinishTaskRequest): Promise<boolean> {
        return new Promise<boolean>(async (resolve, reject) => {
            //TODO Prüfen, ob ID vorhanden ist?
            let taskData = container.get<ITaskData>(DataTypes.TaskData);
            let getTask = new Task();
            getTask.id = obj.id;
            try {
                let task = await taskData.get(getTask);
                if(!task) reject(["Der Task existiert nicht"]);
                else if(task.finished) reject(["Der Task wurde bereits abgeschlossen"]);
            } catch (e) {
                reject(["Es ist ein interner Fehler aufgetreten"]);
            }
            resolve();
        });
    }

}