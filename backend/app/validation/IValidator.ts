export interface IValidator<T> {
    validate(obj: T): Promise<boolean>;
}