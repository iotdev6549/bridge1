import {JasmineLogger} from "./framework/logging/JasmineLogger";
import {ConsoleLogger} from "./framework/logging/ConsoleLogger";
import {ILogger} from "./framework/logging/ILogger";
import {LoggingTypes} from "./framework/logging/LoggingTypes";
import {IDeviceData} from "./data/IDeviceData";
import {DataTypes} from "./data/DataTypes";
import {DeviceData} from "./data/DeviceData";
import {IAuthData} from "./data/IAuthData";
import {AuthData} from "./data/AuthData";
import {ITaskData} from "./data/ITaskData";
import {TaskData} from "./data/TaskData";
import {interfaces} from "inversify";
import Container = interfaces.Container;
import {IMqttHandler, MqttHandlerSymbol} from "./mqtt/IMqttHandler";
import {MqttHandler} from "./mqtt/MqttHandler";
import {mqttConfig, MqttConfigSymbol} from "./config/mqtt.config";
import {IMqttRoutes, MqttRoutesSymbol} from "./mqtt/IMqttRoutes";
import {MqttRoutes} from "./mqtt/MqttRoutes";
import {GetAllDevicesCommandSymbol, IGetAllDevicesCommand} from "./mqtt/commands/interfaces/IGetAllDevicesCommand";
import {GetAllDevicesCommand} from "./mqtt/commands/GetAllDevicesCommand";
import {DataConnectionHandlerSymbol, IDataConnectionHandler} from "./data/IDataConnectionHandler";
import {DataConnectionHandler} from "./data/DataConnectionHandler";
import {IGroupData} from "./data/IGroupData";
import {GroupData} from "./data/GroupData";
import {NewGroupCommandSymbol, INewGroupCommand} from "./mqtt/commands/interfaces/INewGroupCommand";
import {NewGroupCommand} from "./mqtt/commands/NewGroupCommand";

export function bindContainer(container: Container) {

//Logging
    let jasmineLogger = new JasmineLogger([
        new ConsoleLogger(),
        // new DatabaseLogger()
    ]);

    container.bind<ILogger>(LoggingTypes.Logger).toConstantValue(jasmineLogger);


// Data
    container.bind<IDataConnectionHandler>(DataConnectionHandlerSymbol).to(DataConnectionHandler);
    container.bind<IDeviceData>(DataTypes.DeviceData).to(DeviceData);
    container.bind<IAuthData>(DataTypes.AuthData).to(AuthData);
    container.bind<ITaskData>(DataTypes.TaskData).to(TaskData);
    container.bind<IGroupData>(DataTypes.GroupData).to(GroupData);


    //Mqtt
    container.bind(MqttConfigSymbol).toConstantValue(mqttConfig);
    container.bind<IMqttHandler>(MqttHandlerSymbol).toConstantValue(new MqttHandler(mqttConfig));
    container.bind<IMqttRoutes>(MqttRoutesSymbol).to(MqttRoutes);



    //Command
    container.bind<IGetAllDevicesCommand>(GetAllDevicesCommandSymbol).to(GetAllDevicesCommand);
    container.bind<INewGroupCommand>(NewGroupCommandSymbol).to(NewGroupCommand);

}