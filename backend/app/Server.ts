import {LogLevel} from "./framework/logging/LogLevel";
import {Logger} from "./framework/logging/Logger";
import {log} from "./framework/logging/log";
import {IMqttHandler, MqttHandlerSymbol} from "./mqtt/IMqttHandler";
import container from "./inversify.config";
import {BridgeError} from "./models/BridgeError";
import {IMqttRoutes, MqttRoutesSymbol} from "./mqtt/IMqttRoutes";
import {DataConnectionHandlerSymbol, IDataConnectionHandler} from "./data/IDataConnectionHandler";

/**
 * Bridge 1 Server
 * Verbindet sich mit der Datenbank
 * Verbindet sich mit Mqtt Schnittstelle
 */
export class Server {
    private _started = false;
    private _mqttHandler: IMqttHandler;
    private _dataConnectionHandler: IDataConnectionHandler;
    private startupEventCallbacks: (() => void)[] = [];
    private startedEventCallbacks: (() => void)[] = [];
    private errorEventCallbacks: ((err: BridgeError) => void)[] = [];

    constructor() {
        this._mqttHandler = container.get<IMqttHandler>(MqttHandlerSymbol);
        this._dataConnectionHandler = container.get<IDataConnectionHandler>(DataConnectionHandlerSymbol);
    }

    /**
     * Startet den Bridge1 Server
     * Hier werden die Verbindungen zu den verschiedenen Systemen hergestellt
     * Anwendung wird initialisiert
     */
    @log(LogLevel.Method)
    startUp(): Promise<boolean> {
        this.startupEvent();
        return new Promise<boolean>(async (resolve, reject) => {
            if(this._started) return resolve(false);
            //Mit Datenbank verbinden
            try {
                await this._dataConnectionHandler.connect()
            }catch (e) {
                return reject(e);
            }

            //Mit MQTT Schnittstelle verbinden
            try {
                //Verbinden
                await this._mqttHandler.connect();

                //Routen registrieren
                container.get<IMqttRoutes>(MqttRoutesSymbol).register();

                //Anfangen zu lauschen
                this._mqttHandler.startListening();

                //Gehört eigentlich nach Finally, das funktioniert allerdings nicht
                //Fertig gestartet
                this.startedEvent();
                this._started = true;
                resolve(true);
            }
            catch (err) {
                Logger.log(LogLevel.Error, "Konnte keine Verbindung herstellen", "ee2b6d42-42ca-40c8-8cb2-e079870d56ba", err);
                this.errorEvent(new BridgeError(err, "9855e550-f785-4a1f-aa57-a4e869ed9669"));
                return reject(err);
            }
        });

    }

    @log(LogLevel.Method)
    stop() {

    }

    //Events

    /**
     * Registriert einen neuen Handler für das Event StartupEvent
     * @param handler Der zu registrierende Hander (callback)
     */
    registerStartupEventHandler(handler: () => void) {
        this.startupEventCallbacks.push(handler);
    }

    /**
     * Registriert einen neuen Handler für das Event StartedEvent
     * @param handler Der zu registrierende Hander (callback)
     */
    registerStartedEventHandler(handler: () => void) {
        this.startedEventCallbacks.push(handler);
    }

    /**
     * Registriert einen neuen Handler für das Event ErrorEvent
     * @param handler Der zu registrierende Hander (callback)
     */
    registerErrorEventHandler(handler: (err: BridgeError) => void) {
        this.errorEventCallbacks.push(handler);
    }

    private startedEvent() {
        this.startedEventCallbacks.forEach(value => {
            value();
        })
    }

    private startupEvent() {
        this.startupEventCallbacks.forEach(value => {
            value();
        })
    }

    private errorEvent(err: BridgeError) {
        this.errorEventCallbacks.forEach(value => {
            value(err);
        })
    }
}