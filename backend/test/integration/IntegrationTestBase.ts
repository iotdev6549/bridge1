import container from "../../app/inversify.config";
import mongoose from "mongoose";
import {Auth, AuthModel} from "../../app/models/Auth";
import {DeviceModel} from "../../app/models/Device";
import {bindContainer} from "../../app/bindContainer";
import {LoggingTypes} from "../../app/framework/logging/LoggingTypes";
import {ILogger} from "../../app/framework/logging/ILogger";
import {ConsoleLogger} from "../../app/framework/logging/ConsoleLogger";
import {LogLevel} from "../../app/framework/logging/LogLevel";
import {Logger} from "../../app/framework/logging/Logger";
import {Server} from "../../app/Server";
import {TestLogger} from "../../app/framework/logging/TestLogger";
import {DataConnectionHandler} from "../../app/data/DataConnectionHandler";
import {DataConnectionHandlerSymbol, IDataConnectionHandler} from "../../app/data/IDataConnectionHandler";
import {GroupModel} from "../../app/models/Group";

/**
 * Basis-Klasse für Integrationstests
 */
export abstract class IntegrationTestBase {

    /**
     * DataConnectionHandler, der für das Szenario verwendet wird
     */
    static dataConnectionHandler = new DataConnectionHandler();

    /**
     * Server-Instanz, die für das Szenario verwendet wird
     */
    static server: Server = new Server();

    /**
     * Done, welches den aktuellen Test abschließt
     */
    done: any;

    /**
     * Array mit Identifiern für Checkpoints
     */
    checkpoints: string[] = [];

    /**
     * Geht alle gesetzten Checkpoints durch
     * Hierfür muss vorher das Done an Super übergeben werden und das Checkpoints Array gefüllt werden
     * @param identifier
     */
    check(identifier: string) {
        container.get<ILogger>(LoggingTypes.Logger).log(LogLevel.Debug, "Checkpoint erreicht => " + identifier, "459c0a90-e42d-41e4-8370-9fc995467d09", null);
        const index = this.checkpoints.indexOf(identifier, 0);
        if (index > -1) {
            this.checkpoints.splice(index, 1);
        }
        if(this.checkpoints.length == 0) {
            container.get<ILogger>(LoggingTypes.Logger).log(LogLevel.Debug, "Checkpoints done", "e1541a6c-e515-44a1-9798-42f3525a8c23", null);
            this.done();
        } else {
            container.get<ILogger>(LoggingTypes.Logger).log(LogLevel.Debug, "Checkpoints übrig: " + this.checkpoints.length, "e1541a6c-e515-44a1-9798-42f3525a8c23", null);

        }
    }

    /**
     * Aktiviert das Konsolen-Logging für den Testfall
     */
    static activateLogging() {
        console.log("Aktiviere Logging");
        container.rebind<ILogger>(LoggingTypes.Logger).toConstantValue(new ConsoleLogger());
    }


    public static before() {
        process.env.NODE_ENV = 'TEST';
        process.env.DB = "mongodb://localhost:27017/bridge1-integration-test";
        IntegrationTestBase.rebindContainer();

    }

    // noinspection JSMethodCanBeStatic
    public before(done: any) {
        this.done = () => {};
        IntegrationTestBase.initTestData().then(() => {
            done();
        })
    }


    // noinspection JSUnusedGlobalSymbols
    public after () {
        // ...
    }

    // noinspection JSUnusedGlobalSymbols
    public static after () {
        // ...
    }

    private static rebindContainer() {
        container.unbindAll();
        bindContainer(container);
        container.rebind<IDataConnectionHandler>(DataConnectionHandlerSymbol).toConstantValue(IntegrationTestBase.dataConnectionHandler);
        container.rebind<ILogger>(LoggingTypes.Logger).toConstantValue(new TestLogger());
    }

    private static async initTestData() {
        Logger.log(LogLevel.Verbose, "Initialisiere Testdaten", "e7c5a38e-8c8e-4e91-b862-3d28410b16cd", null);

        Logger.log(LogLevel.Verbose, "Initialisiere Testdaten - Leere Testdatenbank...", "bcfeadb9-9900-4090-8795-286d4b961ce4", null);
        await IntegrationTestBase.clearDB();

        Logger.log(LogLevel.Verbose, "Initialisiere Testdaten - Auth-Testdaten...", "aa9839a9-4458-43fc-a120-38504340b48e", null);
        await IntegrationTestBase.insertAuth();

        Logger.log(LogLevel.Verbose, "Initialisiere Testdaten - Device-Testdaten...", "2b5cee32-8f0e-46f4-92f5-66c3062decc0", null);
        await IntegrationTestBase.insertDevice();

        Logger.log(LogLevel.Verbose, "Initialisiere Testdaten - Group-Testdaten...", "76490510-f114-4fe7-8b11-c5ce00a26502", null);
        await IntegrationTestBase.insertGroup();
    }

    private static async clearDB() {
        await mongoose.connection.db.dropDatabase();
    }

    private static async insertAuth() {
        let auth1 = new AuthModel(new Auth());
        auth1.authKey = "testkey";
        await auth1.save();
    }

    private static async insertDevice() {
        let d1 = new DeviceModel();
        d1.name = "TESTDEVICE1";
        d1.identifier = "TESTDEVICE1";
        await d1.save();

        let d2 = new DeviceModel();
        d2.name = "TESTDEVICE2";
        d2.identifier = "TESTDEVICE2";
        await d2.save();

        let d3 = new DeviceModel();
        d3.name = "TESTDEVICE3";
        d3.identifier = "TESTDEVICE3";
        await d3.save();
    }

    private static async insertGroup() {
        let g1 = new GroupModel();
        g1.name = "GROUP1";
        await g1.save();
    }

}