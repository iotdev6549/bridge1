import {suite, test} from "mocha-typescript";
import {IntegrationTestBase} from "../IntegrationTestBase";
import mqtt, {MqttClient} from "mqtt";
import {GroupCommands} from "../../../app/mqtt/topics/commands/GroupCommands";
import {NewGroupRequest} from "../../../app/models/communicationModels/requestModels/group/NewGroupRequest";
import {TestBase} from "../../TestBase";
import {NewGroupResponse} from "../../../app/models/communicationModels/responseModels/group/NewGroupResponse";

let chai = require('chai');
const expect = chai.expect;

@suite() class NewGroupCommand_IntegrationTest extends IntegrationTestBase {

    static async before() {
        IntegrationTestBase.before();
        await IntegrationTestBase.server.startUp();
    }

    before(done: any) {
        super.before(done);
    }

    @test ShouldRunSuccessfully(done: any) {
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new NewGroupRequest();
        req.name = "NEUE GRUPPE";
        req.sender = "TESTDEVICE1";
        mockMqtt.subscribe(`device/TESTDEVICE1/response/NewGroupResponse`);
        mockMqtt.on('message', function (topic, message) {
            let response = JSON.parse(message.toString());
            expect(response.done).to.be.true;
            mockMqtt.end();
            done();
        });

        mockMqtt.publish(GroupCommands.NewGroupCommand, req.toString());
    }

    @test ShouldSendErrorBack_NoSender(done: any) {
        TestBase.registerForLog("782d8ced-b416-4b44-a9c8-fcbb151e76e9", () => {
            done();
        });
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new NewGroupRequest();
        mockMqtt.subscribe(`device/TESTDEVICE1/response/${req.requestId}`);
        mockMqtt.on('message', function () {
            done("Sollte nicht aufgerufen werden");
        });

        mockMqtt.publish(GroupCommands.NewGroupCommand, req.toString());
    }

    @test ShouldSendErrorBack_NoName(done: any) {
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new NewGroupRequest();
        req.sender = "TESTDEVICE1";
        mockMqtt.subscribe(`device/TESTDEVICE1/response/${req.requestId}`);
        mockMqtt.on('message', function (topic, message) {
            let response: NewGroupResponse = JSON.parse(message.toString());
            expect(response.done).to.be.false;
            expect(response.hasError).to.be.true;
            // @ts-ignore
            expect(response.error.origin).to.equal("0af4a6a1-0607-4ce8-a425-da411089cd7a");
            done();
        });

        mockMqtt.publish(GroupCommands.NewGroupCommand, req.toString());
    }

    @test ShouldSendErrorBack_NameAlreadyExists(done: any) {
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new NewGroupRequest();
        req.sender = "TESTDEVICE1";
        req.name = "GROUP1";
        mockMqtt.subscribe(`device/TESTDEVICE1/response/${req.requestId}`);
        mockMqtt.on('message', function (topic, message) {
            let response: NewGroupResponse = JSON.parse(message.toString());
            expect(response.done).to.be.false;
            expect(response.hasError).to.be.true;
            // @ts-ignore
            expect(response.error.origin).to.equal("8916eb70-900a-4c8a-8f42-059212d3c9aa");
            done();
        });

        mockMqtt.publish(GroupCommands.NewGroupCommand, req.toString());
    }

}