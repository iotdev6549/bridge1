import {suite, test, timeout} from "mocha-typescript";
import {IntegrationTestBase} from "../IntegrationTestBase";
import mqtt, {MqttClient} from "mqtt";
import {GetAllDevicesRequest} from "../../../app/models/communicationModels/requestModels/device/GetAllDevicesRequest";
import {DeviceCommands} from "../../../app/mqtt/topics/commands/DeviceCommands";
import {GetAllDevicesResponse} from "../../../app/models/communicationModels/responseModels/device/GetAllDevicesResponse";
import {TestBase} from "../../TestBase";

let chai = require('chai');
const expect = chai.expect;

@suite() class GetAllDevicesCommand_IntegrationTest extends IntegrationTestBase {

    static async before() {
        IntegrationTestBase.before();
        await IntegrationTestBase.server.startUp();
    }

    before(done: any) {
        super.before(done);
    }

    @test(timeout(10000)) ShouldRunSuccessfully(done: any) {
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new GetAllDevicesRequest();
        req.sender = "TESTDEVICE1";
        mockMqtt.subscribe(`device/${req.sender}/response/${req.requestId}`);
        mockMqtt.on('message', function (topic, message) {
            let response: GetAllDevicesResponse  = JSON.parse(message.toString());
            expect(response.done).to.be.true;
            // @ts-ignore
            expect(response.devices.length).to.equal(3);
            done();
        });
        mockMqtt.publish(DeviceCommands.GetAllDevicesCommand, req.toString());
    }


    @test(timeout(10000)) ShouldFail_NoRequest(done: any) {
        TestBase.registerForLog("782d8ced-b416-4b44-a9c8-fcbb151e76e9", () => {
            done();
        });
        let mockMqtt: MqttClient = mqtt.connect("mqtt://localhost");
        let req = new GetAllDevicesRequest();
        mockMqtt.subscribe(`device/${req.sender}/response/${req.requestId}`);
        mockMqtt.on('message', function () {
            done("Sollte nicht aufgerufen werden");
        });
        mockMqtt.publish(DeviceCommands.GetAllDevicesCommand, req.toString());
    }

}