import {suite, test} from "mocha-typescript";
import {Server} from "../../app/Server";
import {MqttConfigSymbol} from "../../app/config/mqtt.config";
import container from "../../app/inversify.config";
import {BridgeError} from "../../app/models/BridgeError";
import {LoggingTypes} from "../../app/framework/logging/LoggingTypes";
import {TestLogger} from "../../app/framework/logging/TestLogger";

let chai = require('chai');
const expect = chai.expect;

@suite class BridgeToBrokerConnectionTest {

    static before() {
        container.rebind(LoggingTypes.Logger).to(TestLogger);
    }

    @test ShouldConnectSuccessfully(done: any) {
        let s = new Server();
        s.startUp().then();
        s.registerStartedEventHandler(() => {
            done();
        });
    }

    @test ShouldNotConnect_WrongAddress(done: any) {
        container.rebind(MqttConfigSymbol).toConstantValue({
           url: "wrongURL"
        });
        let s = new Server();
        s.startUp().then();
        s.registerStartedEventHandler(() => {
            done(new Error("Darf nicht erfolgreich sein"));
        });
        s.registerErrorEventHandler(() => {
            done();
        });
    }
}