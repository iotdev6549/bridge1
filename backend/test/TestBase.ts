import {TaskDataMockup} from "./mocks/TaskDataMockup";
import {DeviceDataMockup} from "./mocks/DeviceDataMockup";
import {AuthDataMockup} from "./mocks/AuthDataMockup";
import container from "../app/inversify.config";
import {ILogger} from "../app/framework/logging/ILogger";
import {LoggingTypes} from "../app/framework/logging/LoggingTypes";
import {TestLogger} from "../app/framework/logging/TestLogger";
import {Task} from "../app/models/Task.";
import {Device} from "../app/models/Device";
import {IDeviceData} from "../app/data/IDeviceData";
import {ITaskData} from "../app/data/ITaskData";
import {DataTypes} from "../app/data/DataTypes";
import {IAuthData} from "../app/data/IAuthData";
import {GroupDataMockup} from "./mocks/GroupDataMockup";
import {IGroupData} from "../app/data/IGroupData";
import {LogLevel} from "../app/framework/logging/LogLevel";
import {CheckupLogger} from "./mocks/CheckupLogger";

/**
 * TestBase für alle Unit-Tests
 */
export abstract class TestBase {

    //Mocks
    static taskDataMock = new TaskDataMockup();
    static deviceDataMock = new DeviceDataMockup();
    static authDataMock = new AuthDataMockup();
    static groupDataMock = new GroupDataMockup();

    public static before(done: any) {
        process.env.NODE_ENV = 'TEST';
        done();
    }

    // noinspection JSMethodCanBeStatic
    public before() {
        //Bindings leeren bevor sie neu gesetzt werden
        //Dadurch werden feste bindings aus anderen Tests entfernt und stören weitere Tests nicht
        container.unbindAll();

        //Mockups binden
        container.bind<ILogger>(LoggingTypes.Logger).toConstantValue(new TestLogger());
        container.bind<ITaskData>(DataTypes.TaskData).toConstantValue(TestBase.taskDataMock);
        container.bind<IDeviceData>(DataTypes.DeviceData).toConstantValue(TestBase.deviceDataMock);
        container.bind<IGroupData>(DataTypes.GroupData).toConstantValue(TestBase.groupDataMock);

        //Test-Mocks leeren, um keine unerwarteten Daten aus vorherigen Tests zu erhalten
        TestBase.taskDataMock.clear();
        TestBase.authDataMock.clear();

        //Base-Testdaten initialisieren
        TestBase.initTestData();
    }


    // noinspection JSUnusedGlobalSymbols
    public after () {
        // ...
    }

    // noinspection JSUnusedGlobalSymbols
    public static after () {
        // ...
    }

    static registerForLog(origin: string, callback: (level: LogLevel, message: string, origin: string, attachment: any) => void) {
        let logger = new CheckupLogger();
        logger.checkFor = origin;
        logger.callback = callback;
        container.rebind(LoggingTypes.Logger).toConstantValue(logger);
    }

    static initTestData() {
        TestBase.insertDevice();
        TestBase.insertTask();
    }

    private static insertDevice() {
        let d1 = new Device();
        d1.id = "TESTDEVICE1";
        d1.name = "TESTDEVICE1";
        d1.identifier = "TESTDEVICE1";
        TestBase.deviceDataMock.devices.push(d1);

        let d2 = new Device();
        d2.id = "TESTDEVICE2";
        d2.name = "TESTDEVICE2";
        d2.identifier = "TESTDEVICE2";
        TestBase.deviceDataMock.devices.push(d2);

        let d3 = new Device();
        d3.id = "TESTDEVICE3";
        d3.name = "TESTDEVICE3";
        d3.identifier = "TESTDEVICE3";
        TestBase.deviceDataMock.devices.push(d3);
    }

    private static insertTask() {
        let t1 = new Task();
        t1.id = "TASK99";
        t1.responsible = "TESTDEVICE1";
        t1.finished = false;
        TestBase.taskDataMock._tasks.push(t1);
    }
}