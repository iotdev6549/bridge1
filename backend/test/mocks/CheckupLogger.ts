import {ILogger} from "../../app/framework/logging/ILogger";
import {LogLevel} from "../../app/framework/logging/LogLevel";

export class CheckupLogger implements ILogger {
    log(level: LogLevel, message: string, origin: string, attachment: any): void {
        if(origin == this.checkFor) this.callback(level, message, origin, attachment);
    }

    registerForLog(origin: string, callback: (level: LogLevel, message: string, origin: string, attachment: any) => void) {
        this.checkFor = origin;
        this.callback = callback;
    }
    callback = (level: LogLevel, message: string, origin: string, attachment: any) => {};
    checkFor = "";
}