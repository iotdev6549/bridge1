import {IAuthData} from "../../app/data/IAuthData";
import {Auth} from "../../app/models/Auth";

export class AuthDataMockup implements IAuthData{
    auth(auth: Auth): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
           if(auth.authKey == "testkey") resolve(true);
           else reject("Falscher Key");
        });
    }

    getKey(): Promise<Auth> {
        return new Promise<Auth>((resolve, reject) => {
            let auth = new Auth();
            auth.authKey = "testkey";
            resolve(auth);
        })
    }


    clear() {

    }
}