import {IGroupData} from "../../app/data/IGroupData";
import {Group} from "../../app/models/Group";

export class GroupDataMockup implements IGroupData{
    createCalled: (group: Group) => void = () => {};


    create(group: Group): Promise<Group> {
        return new Promise<Group>((resolve, reject) => {
            this.createCalled(group);
            resolve();
        })
    }

    delete(group: Group): Promise<void> {
        return new Promise<void>((resolve, reject) => {

        })    }

    findById(id: string): Promise<Group | undefined> {
        return new Promise<Group>((resolve, reject) => {
            if(id == "GROUP1") {
                let g = new Group()
                g.name = "GROUP1";
                resolve(g);
            } else resolve(undefined);
        })
    }

    getAll(): Promise<Group[]> {
        return new Promise<Group[]>((resolve, reject) => {

        })
    }

    update(group: Group): Promise<Group> {
        return new Promise<Group>((resolve, reject) => {

        })
    }

    findByName(name: string): Promise<Group | undefined> {
        return new Promise<Group>((resolve, reject) => {
            if(name == "GROUP1"){
                let g = new Group()
                g.name = name;
                resolve(g);
            } else {
                resolve(undefined);
            }
        })
    }

}