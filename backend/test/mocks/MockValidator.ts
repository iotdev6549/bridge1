import {IValidator} from "../../app/validation/IValidator";
import {TestRequest} from "./TestRequest";

export class MockValidator implements IValidator<TestRequest> {
    validate(obj: TestRequest): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if(obj.valid) resolve();
            else reject();
        })
    }
}