import {IDeviceData} from "../../app/data/IDeviceData";
import {Device} from "../../app/models/Device";

export class DeviceDataMockup implements IDeviceData {

    devices: Device[] = [];

    create(device: Device): Promise<Device> {
        return new Promise<Device>((resolve, reject) => {
            this.devices.push(device);
            resolve(device);
        });
    }

    get(device: Device): Promise<Device | undefined> {
        return new Promise((resolve, reject) => {
            this.devices.forEach(d => {
                if(d.identifier == device.identifier) resolve(d);
            });
            resolve(undefined);
        });
    }

    getMultiple(devices: Device[]): Promise<Device[]> {
        return new Promise<Device[]>((resolve, reject) => {

        });
    }

    updateState(device: Device): Promise<Device> {
        return new Promise<Device>((resolve, reject) => {

        });
    }

    clear() {
        this.devices = [];
    }

    getByIdentifier(identifier: string): Promise<Device | undefined> {
        return new Promise((resolve, reject) => {
            this.devices.forEach(d => {
                if(d.identifier == identifier) resolve(d);
            });
            resolve(undefined);
        });    }

    getAll(): Promise<Device[]> {
        return new Promise<Device[]>((resolve, reject) => {
            resolve(this.devices);
        })
    }

}