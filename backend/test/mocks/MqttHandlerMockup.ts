import {IMqttHandler} from "../../app/mqtt/IMqttHandler";

export class MqttHandlerMockup implements IMqttHandler{
    publishCalled: (topic: string, message: string, options: any, callback: () => void) => void = () => {};

    connect(): Promise<void> {
        return new Promise<void>((resolve) => {
            resolve();
        })
    }

    publish(topic: string, message: string, options: any, callback: () => void): void {
        this.publishCalled(topic, message, options, callback);
    }

    registerRoute(route: string, callback: any): void {
    }


    startListening(): void {
    }

    clear() {
        this.publishCalled = () => {}
    }

}