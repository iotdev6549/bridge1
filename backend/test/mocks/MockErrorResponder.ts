import {IErrorResponder} from "../../app/framework/logic/IErrorResponder";
import {RequestParent} from "../../app/models/communicationModels/requestModels/RequestParent";
import {BridgeError} from "../../app/models/BridgeError";

export class MockErrorResponder implements IErrorResponder{
    respond(request: RequestParent, error: BridgeError): void {
    }

}