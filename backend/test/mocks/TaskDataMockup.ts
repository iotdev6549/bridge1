import {ITaskData} from "../../app/data/ITaskData";
import {Task} from "../../app/models/Task.";
const uuidv4 = require('uuid/v4');


export class TaskDataMockup implements ITaskData{
    _tasks: Task[] = [];
    _newTaskCallback = (task: Task) => {};
    _finishTaskCallback = (task: Task) => {};
    create(task: Task): Promise<Task> {
        return new Promise<Task>((resolve, reject) => {
            task.created = new Date();
            task.id = uuidv4();
            this._tasks.push(task);
            this._newTaskCallback(task);
            resolve(task);
        })
    }

    finishTask(task: Task): Promise<Task> {
        return new Promise<Task>((resolve, reject) => {
            this._tasks.forEach(value => {
                if(value.id == task.id) {
                    value.finished = true;
                    this._finishTaskCallback(value);
                    resolve(value);
                }
            });
            reject("Der Task konnte nicht gefunden werden");
        })
    }

    getNext(responsible: string): Promise<Task> {
        return new Promise<Task>((resolve, reject) => {

        })
    }

    get(task: Task): Promise<Task | null> {
        return new Promise<Task>((resolve, reject) => {
            this._tasks.forEach(value => {
                if (value.id == task.id) resolve(value);
            });
            resolve(undefined);
        })
    }

    clear() {
        this._tasks = [];
        this._newTaskCallback = (task: Task) => {};
        this._finishTaskCallback = (task: Task) => {};
    }

}