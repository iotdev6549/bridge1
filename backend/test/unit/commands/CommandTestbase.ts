import container from "../../../app/inversify.config";
import {DeviceDataMockup} from "../../mocks/DeviceDataMockup";
import {IDeviceData} from "../../../app/data/IDeviceData";
import {DataTypes} from "../../../app/data/DataTypes";
import {LoggingTypes} from "../../../app/framework/logging/LoggingTypes";
import {TestLogger} from "../../../app/framework/logging/TestLogger";
import {Device} from "../../../app/models/Device";
import {MqttHandlerMockup} from "../../mocks/MqttHandlerMockup";
import {IMqttHandler, MqttHandlerSymbol} from "../../../app/mqtt/IMqttHandler";
import {ILogger} from "../../../app/framework/logging/ILogger";
import {ConsoleLogger} from "../../../app/framework/logging/ConsoleLogger";
import {GroupDataMockup} from "../../mocks/GroupDataMockup";
import {IGroupData} from "../../../app/data/IGroupData";
import {LogLevel} from "../../../app/framework/logging/LogLevel";
import {CheckupLogger} from "../../mocks/CheckupLogger";

export class CommandTestbase {
    static deviceDataMock = new DeviceDataMockup();
    static groupDataMock = new GroupDataMockup();
    static mqttHandlerMock = new MqttHandlerMockup();

    static before() {

    }

    before() {
        container.unbindAll();
        container.bind(LoggingTypes.Logger).to(TestLogger);
        container.bind<IDeviceData>(DataTypes.DeviceData).toConstantValue(CommandTestbase.deviceDataMock);
        container.bind<IGroupData>(DataTypes.GroupData).toConstantValue(CommandTestbase.groupDataMock);
        container.bind<IMqttHandler>(MqttHandlerSymbol).toConstantValue(CommandTestbase.mqttHandlerMock);
        CommandTestbase.initTestData();
    }

    after() {
        container.unbindAll();
        CommandTestbase.mqttHandlerMock.clear();
        CommandTestbase.deviceDataMock.clear();
    }

    activateLogging(){
        container.rebind<ILogger>(LoggingTypes.Logger).to(ConsoleLogger);
    }

    static registerForLog(origin: string, callback: (level: LogLevel, message: string, origin: string, attachment: any) => void) {
        let logger = new CheckupLogger();
        logger.checkFor = origin;
        logger.callback = callback;
        container.rebind(LoggingTypes.Logger).toConstantValue(logger);
    }

    private static initTestData() {
        CommandTestbase.deviceDataMock.clear();
        let d1 = new Device();
        d1.identifier = "TESTDEVICE1";
        d1.name = "TESTDEVICE1";

        let d2 = new Device();
        d2.identifier = "TESTDEVICE2";
        d2.name = "TESTDEVICE2";

        let d3 = new Device();
        d3.identifier = "TESTDEVICE3";
        d3.name = "TESTDEVICE3";

        CommandTestbase.deviceDataMock.devices.push(d1);
        CommandTestbase.deviceDataMock.devices.push(d2);
        CommandTestbase.deviceDataMock.devices.push(d3);

    }



}