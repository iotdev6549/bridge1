import {suite, test} from "mocha-typescript";
import {CommandTestbase} from "./CommandTestbase";
import {GetAllDevicesCommand} from "../../../app/mqtt/commands/GetAllDevicesCommand";
import {DeviceCommands} from "../../../app/mqtt/topics/commands/DeviceCommands";
import {CheckupLogger} from "../../mocks/CheckupLogger";
import {LoggingTypes} from "../../../app/framework/logging/LoggingTypes";
import container from "../../../app/inversify.config";
import {NewGroupCommand} from "../../../app/mqtt/commands/NewGroupCommand";
import {NewGroupRequest} from "../../../app/models/communicationModels/requestModels/group/NewGroupRequest";
import {Group} from "../../../app/models/Group";

let chai = require('chai');
const expect = chai.expect;

@suite class NewGroupCommandTest extends CommandTestbase{
    static before() {
        CommandTestbase.before();
    }

    before() {
        super.before();
    }

    @test ShouldRunSuccessfully(done: any) {
        let command = new NewGroupCommand(CommandTestbase.mqttHandlerMock, CommandTestbase.groupDataMock);
        let req = new NewGroupRequest();
        req.name = "NEUEGRUPPE";
        req.sender = "TESTDEVICE1";
        //TODO Gruppen und Devices füllen

        CommandTestbase.groupDataMock.createCalled = (group: Group) => {
            done();
        };
        command.execute(req);
    }

    // @test ShouldFail_ValidationFail_EmptyRequest(done: any) {
    //     let logger = new CheckupLogger();
    //     logger.checkFor = "80657c0a-0d08-4aaf-a290-04a44dd66224";
    //     logger.done = done;
    //     container.rebind(LoggingTypes.Logger).toConstantValue(logger);
    //     let command = new GetAllDevicesCommand(CommandTestbase.mqttHandlerMock, CommandTestbase.deviceDataMock);
    //     let req = {
    //     };
    //     command.execute(req);
    // }

}