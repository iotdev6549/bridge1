import {suite, test} from "mocha-typescript";
import {CommandTestbase} from "./CommandTestbase";
import {GetAllDevicesCommand} from "../../../app/mqtt/commands/GetAllDevicesCommand";
import {DeviceCommands} from "../../../app/mqtt/topics/commands/DeviceCommands";
import {CheckupLogger} from "../../mocks/CheckupLogger";
import {LoggingTypes} from "../../../app/framework/logging/LoggingTypes";
import container from "../../../app/inversify.config";
import {GetAllDevicesRequest} from "../../../app/models/communicationModels/requestModels/device/GetAllDevicesRequest";
import {GetAllDevicesResponse} from "../../../app/models/communicationModels/responseModels/device/GetAllDevicesResponse";
import {IntegrationTestBase} from "../../integration/IntegrationTestBase";

let chai = require('chai');
const expect = chai.expect;

@suite class GetAllDevicesCommandTest extends CommandTestbase{
    static before() {
        CommandTestbase.before();
    }

    before() {
        super.before();
    }

    @test ShouldRunSuccessfully(done: any) {
        let command = new GetAllDevicesCommand(CommandTestbase.mqttHandlerMock, CommandTestbase.deviceDataMock);
        let req = new GetAllDevicesRequest();
        req.sender = "TESTDEVICE1";
        CommandTestbase.mqttHandlerMock.publishCalled = (topic, message, options, callback) => {
            expect(topic).to.equal(`device/TESTDEVICE1/response/${req.requestId}`);
            let response: GetAllDevicesResponse = JSON.parse(message);
            expect(response.done).to.be.true;
            expect(response.hasError).to.be.false;
            // @ts-ignore
            expect(response.devices.length).to.equal(3);
            done();
        };
        command.execute(req);
    }
    //
    // @test ShouldFail_ValidationFail_EmptyRequest(done: any) {
    //     let command = new GetAllDevicesCommand(CommandTestbase.mqttHandlerMock, CommandTestbase.deviceDataMock);
    //     let req = new GetAllDevicesRequest();
    //     CommandTestbase.mqttHandlerMock.publishCalled = (topic, message, options, callback) => {
    //         expect(topic).to.equal(`device/TESTDEVICE1/response/${req.requestId}`);
    //         let response: GetAllDevicesResponse = JSON.parse(message);
    //         console.log(response)
    //         expect(response.done).to.be.false;
    //         expect(response.hasError).to.be.true;
    //         done();
    //     };
    //
    //     command.execute(req);
    // }

}