import {suite, test} from "mocha-typescript";
import {DataTestBase} from "./DataTestBase";
import {GroupData} from "../../../app/data/GroupData";
import {Group, GroupModel} from "../../../app/models/Group";

let chai = require('chai');
const expect = chai.expect;

@suite class GroupDataTest extends DataTestBase {
    static async before() {
        await DataTestBase.before();
    }

    async before() {
        await super.before();
    }

    createTestGroups(): Promise<string[]> {
        return new Promise<string[]>(async (resolve, reject) => {
            let g1 = new GroupModel();
            g1.name = "TESTGRUPPE1";
            let g1Done = await g1.save();

            let g2 = new GroupModel();
            g2.name = "TESTGRUPPE2";
            let g2Done = await g2.save();

            resolve([
                g1Done._id,
                g2Done._id
            ]);
        });

    }

    @test CreateGroupShouldSucceed(done: any) {
        this.createTestGroups().then((ids: string[]) => {
            let data = new GroupData();
            let group = new Group();
            group.name = "NEUE_GRUPPE";
            group.subGroups = ids;
            data.create(group).then(value => {
                GroupModel.find(value).then(value => {
                    expect(value.length).to.equal(1);
                    let group = value[0];
                    expect(group.name).to.equal("NEUE_GRUPPE");
                    expect(group.subGroups).to.not.be.undefined;
                    // @ts-ignore
                    expect(group.subGroups.length).to.equal(2);
                    done();
                });

            })
        });

    }



}