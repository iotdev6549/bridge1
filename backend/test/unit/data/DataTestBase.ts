import container from "../../../app/inversify.config";
import {DataConnectionHandler} from "../../../app/data/DataConnectionHandler";
import mongoose from "mongoose";
import {DeviceModel} from "../../../app/models/Device";
import {GroupModel} from "../../../app/models/Group";
import {ILogger} from "../../../app/framework/logging/ILogger";
import {LoggingTypes} from "../../../app/framework/logging/LoggingTypes";
import {ConsoleLogger} from "../../../app/framework/logging/ConsoleLogger";
import {TestLogger} from "../../../app/framework/logging/TestLogger";


/**
 * TestBase für DataTests
 * Bietet die Grundlage verschiedener Tests des Data-Layers
 * @tutorial ES WIRD EINE MONGODB INSTANZ BENÖTIGT
 */
export class DataTestBase {

    /**
     * Initialisiert alle Tests
     * Stellt Datenbankverbindung her und entbindet alle Container
     */
    static async before() {
        process.env.NODE_ENV = 'TEST';
        process.env.DB = "mongodb://localhost:27017/bridge1-data-test";
        container.unbindAll();
        container.bind<ILogger>(LoggingTypes.Logger).toConstantValue(new TestLogger());


        //Datenbankverbindung aufbauen
        await new DataConnectionHandler().connect();
    }

    /**
     * Initialisiert Testdaten
     */
    async before() {
        await DataTestBase.initTestData();
    }
    private static async initTestData() {
        await DataTestBase.clearDb();
        await DataTestBase.initDeviceData();
        await DataTestBase.initGroupData();
    }

    static async initDeviceData() {
        let d1 = new DeviceModel();
        d1.identifier = "TESTDEVICE1";
        d1.name = "TESTDEVICE1";

        let d2 = new DeviceModel();
        d2.identifier = "TESTDEVICE2";
        d2.name = "TESTDEVICE2";

        await d1.save();
        await d2.save();
    }

    static async initGroupData() {
        let g1 = new GroupModel();
        g1.name = "GROUP1";

        await g1.save();
    }

    static async clearDb() {
        await mongoose.connection.db.dropDatabase();
    }


}