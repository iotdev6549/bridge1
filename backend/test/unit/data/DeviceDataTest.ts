import {suite, test} from "mocha-typescript";
import {DeviceData} from "../../../app/data/DeviceData";
import {DataTestBase} from "./DataTestBase";
import {Device} from "../../../app/models/Device";

let chai = require('chai');
const expect = chai.expect;

@suite class DeviceDataTest extends DataTestBase {
    static async before() {
        await DataTestBase.before();
    }

    async before() {
        await super.before();
    }

    @test CreateDevice_ShouldCreateDevice(done: any) {
        let data = new DeviceData();
        let device = new Device();
        device.name = "NEUESDEVICE";
        device.identifier = "NEUESDEVICE";
        data.create(device).then(value => {
            expect(value.identifier).to.equal("NEUESDEVICE");
            done();
        });

    }

    @test GetByIdentifier_ShouldGetSingleDevice(done: any) {
        let data = new DeviceData();
        data.getByIdentifier("TESTDEVICE1").then(value => {
            if(value != undefined) {
                expect(value.name).to.equal("TESTDEVICE1");
                done();
            }
        });

    }

    @test GetByIdentifier_ShouldGetUndefined(done: any) {
        let data = new DeviceData();
        data.getByIdentifier("WRONGDEVICE").then(value => {
            if(value == undefined) done();
        });

    }
}