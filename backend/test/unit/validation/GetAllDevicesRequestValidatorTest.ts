import {suite} from "mocha-typescript";
import {TestBase} from "../../TestBase";
import {GetAllDevicesRequestValidator} from "../../../app/validation/device/GetAllDevicesRequestValidator";
import {GetAllDevicesRequest} from "../../../app/models/communicationModels/requestModels/device/GetAllDevicesRequest";

@suite class GetAllDevicesRequestValidatorTest extends TestBase {

    static before(done: any) {
        TestBase.before(done);
    }

    before() {
        super.before();
    }

    @test ShouldGoThroughValidation(done: any) {
        let val = new GetAllDevicesRequestValidator();
        let request = new GetAllDevicesRequest();
        request.sender = "TESTDEVICE1";
        val.validate(request).then(() => {
            done();
        }).catch(reason => {
            done("Sollte keine Fehler werfen");
        });
    }

    @test ValidationShouldFail_NoSender(done: any) {
        let val = new GetAllDevicesRequestValidator();
        let request = new GetAllDevicesRequest();
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen");
        }).catch(reason => {
            done();
        });
    }

    @test ValidationShouldFail_SenderDoesntExist(done: any) {
        let val = new GetAllDevicesRequestValidator();
        let request = new GetAllDevicesRequest();
        request.sender = "FALSCHERSENDER";
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch(reason => {
            done();
        })
    }

}