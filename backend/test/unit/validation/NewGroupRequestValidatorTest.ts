import {suite} from "mocha-typescript";
import {TestBase} from "../../TestBase";
import {NewGroupRequest} from "../../../app/models/communicationModels/requestModels/group/NewGroupRequest";
import {NewGroupRequestValidator} from "../../../app/validation/group/NewGroupRequestValidator";

let chai = require('chai');
const expect = chai.expect;

@suite class NewGroupRequestValidatorTest extends TestBase {

    static before(done: any) {
        TestBase.before(done);
    }

    before() {
        super.before();
    }

    @test ShouldGoThroughValidation(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        request.sender = "TESTDEVICE1";
        request.name = "TESTGRUPPE";
        request.subGroups = ["GROUP1"];
        request.devices = ["TESTDEVICE1"];
        val.validate(request).then(() => {
            done();
        }).catch(reason => {
            done("Sollte keine Fehler werfen");
        });
    }

    @test ValidationShouldFail_NoSender(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch((err) => {
            expect(err.origin).to.equal("e8b4ac0e-9696-4f71-9ac9-0c703f91067c");
            done();
        })
    }

    @test ValidationShouldFail_SenderDoesntExist(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        request.sender = "FALSCHER SENDER";
        request.name = "Gruppe";
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch((err) => {
            expect(err.origin).to.equal("a450dc27-4aa3-46e4-9d2d-22ac971cdb99");
            done();
        })
    }


    @test ValidationShouldFail_NoName(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        request.sender = "TESTDEVICE1";
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch((err) => {
            expect(err.origin).to.equal("0af4a6a1-0607-4ce8-a425-da411089cd7a");
            done();
        })
    }

    @test ValidationShouldFail_SubGroupDoenstExist(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        request.sender = "TESTDEVICE1";
        request.name = "Name";
        request.subGroups = ["NICHTVORHANDEN"];
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch((err) => {
            expect(err.origin).to.equal("6ce92b2f-c982-43f5-aa1b-04783ec6e816");
            done();
        })
    }

    @test ValidationShouldFail_DeviceDoesntExist(done: any) {
        let val = new NewGroupRequestValidator();
        let request = new NewGroupRequest();
        request.sender = "TESTDEVICE1";
        request.name = "Name";
        request.devices = ["NICHT VORHANDEN"];
        val.validate(request).then(() => {
            done("Sollte nicht durchlaufen")
        }).catch((err) => {
            expect(err.origin).to.equal("315d7bdb-0c58-420a-a40f-9f84d634656e");
            done();
        })
    }

}