import {suite, test} from "mocha-typescript";
import {TestBase} from "../../TestBase";
import {requestWithValidBody} from "../../../app/validation/requestWithValidBody";
import {requestModel} from "../../../app/framework/base/decorators/requestModel";
import {ILogger} from "../../../app/framework/logging/ILogger";
import container from "../../../app/inversify.config";
import {LoggingTypes} from "../../../app/framework/logging/LoggingTypes";
import {TestRequest} from "../../mocks/TestRequest";
import {MockValidator} from "../../mocks/MockValidator";
import {CheckupLogger} from "../../mocks/CheckupLogger";
import {CommandTestbase} from "../commands/CommandTestbase";
import Test = Mocha.Test;
import {MockErrorResponder} from "../../mocks/MockErrorResponder";

let chai = require('chai');
const expect = chai.expect;

@suite class RequestWithValidBodyTest extends TestBase {
    static before(done: any) {
        TestBase.before(done);
    }

    before() {
        super.before();
    }

    @test ShouldExecuteMethod(done: any) {
        let req = new TestRequest();
        req.valid = true;
        this.testfunc(req ,() => {
            done();
        });
    }

    @test ShouldNotExecuteMethod(done: any) {
        let req = new TestRequest();
        req.valid = false;

        TestBase.registerForLog("80657c0a-0d08-4aaf-a290-04a44dd66224", () => {
            done();
        });

        this.testfunc(req ,() => {
            done("Sollte nicht ausgeführt werden");
        });
    }

    @test ShouldDoNothing_NoRequestFound(done: any) {
        let req = new TestRequest();
        req.valid = false;

        TestBase.registerForLog("d5badbb3-277e-421d-ac65-6cf3c1968d84", () => {
            done();
        });

        this.testfunc_noRequest(req ,() => {
            done("Sollte nicht ausgeführt werden");
        });
    }

    @requestWithValidBody<TestRequest>(new MockValidator(), new MockErrorResponder())
    testfunc(@requestModel req: TestRequest, cb: () => void) {
        cb();
    }

    @requestWithValidBody<TestRequest>(new MockValidator(), new MockErrorResponder())
    testfunc_noRequest(req: TestRequest, cb: () => void) {
        cb();
    }


}




