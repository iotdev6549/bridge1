# Bridge1

##Entwicklung
###Quellen
* **NodeJS + TypeScript:** https://medium.com/javascript-in-plain-english/typescript-with-node-and-express-js-why-when-and-how-eb6bc73edd5d
* **Dependency-Injection:** http://inversify.io/
* **Unit-Test:** https://scotch.io/tutorials/test-a-node-restful-api-with-mocha-and-chai
* **Mocking:** https://spin.atomicobject.com/2018/06/13/mock-typescript-modules-sinon/
* **rethinkdb** https://www.rethinkdb.com/docs/nodejs/


##Produktivsetzung

###SSH Zugang einrichten
Datei mit dem Namen "ssh" im root der Speicherkarte anlegen

### RethinkDB installieren
https://www.rethinkdb.com/docs/nodejs/
### NodeJS installieren
https://thisdavej.com/beginners-guide-to-installing-node-js-on-a-raspberry-pi/

### Apache installieren
https://www.raspberrypi.org/documentation/remote-access/web-server/apache.md